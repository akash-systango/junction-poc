package JunctionPackage;


import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class AddSensor {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		WebDriver driver=new FirefoxDriver();
		driver.get("https://dashboard-staging.junction.com/#/signin?token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJha2FzaEBzeXN0YW5nby5jb20iLCJleHAiOjE0NzM0OTg5MzMsImlzcyI6Imh0dHBzOi8vY2xvdWQuanVuY3Rpb24uY29tLyIsImlhdCI6MTQ3MjIwMjkzMywidHlwIjoiSldUIiwiYXVkIjoiYUxiS0xYSFNjeW94Tmxhd1ExTDlPYlNaSXZPa29HRkUifQ.FoCeVUsfpl5272apsxfhoNHJwWFngb5b9Wk5ZAcJEhQ");
		driver.manage().timeouts().implicitlyWait(50,TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.findElement(By.xpath("//h1[text()='Properties']"));
        List<WebElement> lst=driver.findElements(By.xpath("//div[@class='src-components-media-ImageRatio-stretch-l_6CY']"));
        lst.get(0).click();
		driver.manage().timeouts().implicitlyWait(50,TimeUnit.SECONDS);
		List<WebElement> lst1=driver.findElements(By.xpath("//div[@class='src-components-sensors-LargeSensorCard-empty-38OW_']"));
		lst1.get(1).click();
		driver.findElement(By.xpath("//h3[contains(text(),'Temperature Sensor')]")).click();
		driver.findElement(By.xpath("//button[text()='Next']")).click();
		driver.findElement(By.xpath("//input[@placeholder='Sensor Name']")).sendKeys("Temp Sensor");
		driver.findElement(By.xpath("//div[text()='Select']/following-sibling::i")).click();
		driver.findElement(By.xpath("//div[text()='Air Temperature']")).click();
	    List<WebElement> lst2=driver.findElements(By.xpath("//input[@name='port']"));
        lst2.get(0).click();
        driver.findElement(By.xpath("//div[text()='10 minutes']/following-sibling::i")).click();
        driver.findElement(By.xpath("//div[text()='1 hour']")).click();
        driver.findElement(By.xpath("//button[text()='Save']")).click();
        driver.quit();

	}

}
