package JunctionPackage;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;

public class DragDrop {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		WebDriver driver=new FirefoxDriver();
		driver.get("https://dashboard-staging.junction.com/#/signin?token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJha2FzaEBzeXN0YW5nby5jb20iLCJleHAiOjE0NzM0OTg5MzMsImlzcyI6Imh0dHBzOi8vY2xvdWQuanVuY3Rpb24uY29tLyIsImlhdCI6MTQ3MjIwMjkzMywidHlwIjoiSldUIiwiYXVkIjoiYUxiS0xYSFNjeW94Tmxhd1ExTDlPYlNaSXZPa29HRkUifQ.FoCeVUsfpl5272apsxfhoNHJwWFngb5b9Wk5ZAcJEhQ");
		driver.manage().timeouts().implicitlyWait(50,TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.findElement(By.xpath("//h1[text()='Properties']"));
		List<WebElement> lst= driver.findElements(By.xpath("//div[@class='src-components-media-ImageRatio-stretch-l_6CY']"));
		lst.get(0).click();
		driver.findElement(By.xpath("//i[@class='blue settings icon']")).click();
		driver.findElement(By.xpath("//div[text()='Property']")).click();
		driver.findElement(By.xpath("//a[text()='Users and Escalation']")).click();
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,250)", "");
		driver.findElement(By.xpath("//h3[contains(text(),'Drag & Drop User')]"));
		WebElement Ele=driver.findElement(By.xpath("//div[@id='copy_from_list']/div[@id='179']"));
		WebElement Ele2=driver.findElement(By.xpath(".//*[@id='level-1,priority-1']"));
		Actions act=new Actions(driver);
		act.clickAndHold(Ele);
		act.moveToElement(Ele2);
		act.release().build().perform();

	}

}
