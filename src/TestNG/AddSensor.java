package TestNG;

import static org.testng.Assert.*;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.junit.AfterClass;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class AddSensor {
	public static WebDriver driver;
	@BeforeClass
	public void initialSetup(){
		driver=new FirefoxDriver();
		driver.get("https://dashboard-staging.junction.com/#/signin?token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJha2FzaEBzeXN0YW5nby5jb20iLCJleHAiOjE0NzM0OTg5MzMsImlzcyI6Imh0dHBzOi8vY2xvdWQuanVuY3Rpb24uY29tLyIsImlhdCI6MTQ3MjIwMjkzMywidHlwIjoiSldUIiwiYXVkIjoiYUxiS0xYSFNjeW94Tmxhd1ExTDlPYlNaSXZPa29HRkUifQ.FoCeVUsfpl5272apsxfhoNHJwWFngb5b9Wk5ZAcJEhQ");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(120,TimeUnit.SECONDS);
			driver.findElement(By.xpath("//h1[text()='Properties']"));
        List<WebElement> lst=driver.findElements(By.xpath("//div[@class='src-components-media-ImageRatio-stretch-l_6CY']"));
        lst.get(0).click();
        System.out.println("Initial setup Completes");
		
	}
	
	
  @Test
  public void VerifyAddSenser() throws IOException, InterruptedException {
	 	  List<WebElement> lst1=driver.findElements(By.xpath("//div[@class='src-components-sensors-LargeSensorCard-empty-38OW_']"));
		lst1.get(0).click();
	  driver.findElement(By.xpath("//h3[contains(text(),'Temperature Sensor')]")).click();
		driver.findElement(By.xpath("//button[text()='Next']")).click();
		driver.findElement(By.xpath("//input[@placeholder='Sensor Name']")).sendKeys("New Sensor 12");
		driver.findElement(By.xpath("//div[text()='Select']/following-sibling::i")).click();
		driver.findElement(By.xpath("//div[text()='Air Temperature']")).click();
	    List<WebElement> lst2=driver.findElements(By.xpath("//input[@name='port']"));
      lst2.get(0).click();
      driver.findElement(By.xpath("//div[text()='10 minutes']/following-sibling::i")).click();
      driver.findElement(By.xpath("//div[text()='2 hours']")).click();
      driver.findElement(By.xpath("//button[text()='Save']")).click();
      Thread.sleep(5000);
      WebElement ele3= driver.findElement(By.xpath("//h2[contains(text(),'systango')]"));
      captureScreenshot("AddSensorImage");
      assertTrue(ele3.isDisplayed());
      System.out.println("External Sensor Added");
		
  
  }


@AfterClass
public void CloseSetup() {
	driver.quit();
	
}





public static void captureScreenshot(String fileName) throws IOException {
  	
    TakesScreenshot ts= (TakesScreenshot)driver;
    File src=ts.getScreenshotAs(OutputType.FILE); 
    FileUtils.copyFile(src, new File("./test-output/ScreenCapture/"+ fileName+".png"));

}


}



