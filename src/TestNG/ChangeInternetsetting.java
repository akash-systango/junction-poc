 package TestNG;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;

import static org.testng.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterClass;


public class ChangeInternetsetting {
	static WebDriver driver;
	 @BeforeClass
	  public void initialsetup() {
		 driver=new FirefoxDriver();
			driver.get("https://dashboard-staging.junction.com/#/signin?token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJha2FzaEBzeXN0YW5nby5jb20iLCJleHAiOjE0NzM0OTg5MzMsImlzcyI6Imh0dHBzOi8vY2xvdWQuanVuY3Rpb24uY29tLyIsImlhdCI6MTQ3MjIwMjkzMywidHlwIjoiSldUIiwiYXVkIjoiYUxiS0xYSFNjeW94Tmxhd1ExTDlPYlNaSXZPa29HRkUifQ.FoCeVUsfpl5272apsxfhoNHJwWFngb5b9Wk5ZAcJEhQ");
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(50,TimeUnit.SECONDS);
				driver.findElement(By.xpath("//h1[text()='Properties']"));
	        List<WebElement> lst=driver.findElements(By.xpath("//div[@class='src-components-media-ImageRatio-stretch-l_6CY']"));
	        lst.get(0).click();
	        
			
	  }
	
  @Test
  public void VerifyChangeInternetSetting() throws IOException {
	  driver.manage().timeouts().implicitlyWait(50,TimeUnit.SECONDS);
		driver.findElement(By.xpath("//i[@class='blue settings icon']")).click();
		driver.findElement(By.xpath("//div[contains(text(),'Change Internet Connection')]")).click();
		driver.findElement(By.xpath("//button[@class='ui primary undefined undefined button right floated' and text()='Yes']")).click();
		driver.findElement(By.xpath("//a[text()='Cellular']")).click();
		driver.findElement(By.xpath("//button[text()='Done']")).click();
		driver.manage().timeouts().implicitlyWait(50,TimeUnit.SECONDS);
		WebElement ele3= driver.findElement(By.xpath("//h2[contains(text(),'systango')]"));
		captureScreenshot("ChangeInternet");
		assertTrue(ele3.isDisplayed());
		System.out.println("Internet setting change");
  }
 

  @AfterClass
  public void Closesetup() {
	  driver.quit();
  }

  public static void captureScreenshot(String fileName) throws IOException {
	  	
	    TakesScreenshot ts= (TakesScreenshot)driver;
	    File src=ts.getScreenshotAs(OutputType.FILE); 
	    FileUtils.copyFile(src, new File("./test-output/ScreenCapture/"+ fileName+".png"));

	}

}

