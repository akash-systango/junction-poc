package TestNG;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;

import static org.testng.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.AfterClass;


public class ConfiguredProperty {
	public static WebDriver driver;
	
 @BeforeClass  public void Initialsetup()
	 {
	 driver=new FirefoxDriver();
		driver.get("https://dashboard-staging.junction.com/#/signin?token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJha2FzaEBzeXN0YW5nby5jb20iLCJleHAiOjE0NzM0OTg5MzMsImlzcyI6Imh0dHBzOi8vY2xvdWQuanVuY3Rpb24uY29tLyIsImlhdCI6MTQ3MjIwMjkzMywidHlwIjoiSldUIiwiYXVkIjoiYUxiS0xYSFNjeW94Tmxhd1ExTDlPYlNaSXZPa29HRkUifQ.FoCeVUsfpl5272apsxfhoNHJwWFngb5b9Wk5ZAcJEhQ");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(50,TimeUnit.SECONDS);
			driver.findElement(By.xpath("//h1[text()='Properties']"));
     List<WebElement> lst=driver.findElements(By.xpath("//div[@class='src-components-media-ImageRatio-stretch-l_6CY']"));
     lst.get(0).click();
    
	  }
 
 
  @Test
  public void VerifyConfiguredproperty() throws IOException {
	  driver.manage().timeouts().implicitlyWait(50,TimeUnit.SECONDS);
		driver.findElement(By.xpath("//div[text()='Temperature']")).click();
	    driver.findElement(By.xpath(".//*[@id='outlet']/div/div[2]/div/div[3]/div/div[2]/div/div/div[2]/div/div/div[1]/div/form/div[2]/div[3]"));
		WebElement ele=driver.findElement(By.xpath(".//*[@id='outlet']/div/div[2]/div/div[3]/div/div[2]/div/div/div[2]/div/div/div[1]/div/form/div[2]/div[3]/div/div[2]"));
		WebElement ele1=driver.findElement(By.xpath(".//*[@id='outlet']/div/div[2]/div/div[3]/div/div[2]/div/div/div[2]/div/div/div[1]/div/form/div[2]/div[3]/div/div[4]/span[1]"));
		Actions act = new Actions(driver);
      act.clickAndHold(ele);
      act.moveToElement(ele1);
      act.release(ele);
      act.build().perform();
      driver.manage().timeouts().implicitlyWait(50,TimeUnit.SECONDS);
      driver.findElement(By.xpath("//div[text()='1 hour']/following-sibling::i")).click();
     Actions act1 = new Actions(driver);
     act1.click(driver.findElement(By.xpath("//div[@data-text='2 hours']"))).build().perform();
     driver.findElement(By.xpath("//div[@class='ui toggle checkbox']/label[text()='Smart Alert']")).click();
     driver.findElement(By.xpath("//a[text()='Save']")).click();
     driver.manage().timeouts().implicitlyWait(50,TimeUnit.SECONDS);
     WebElement ele3= driver.findElement(By.xpath("//h2[contains(text(),'systango')]"));
     captureScreenshot("ConfiguredProperty");
     assertTrue(ele3.isDisplayed());
     System.out.println("Sensor Value Updated");
  }
 

  @AfterClass
  public void afterClass() {
	  driver.quit();
	  
	 
  }
  public static void captureScreenshot(String fileName) throws IOException {
	  	
	    TakesScreenshot ts= (TakesScreenshot)driver;
	    File src=ts.getScreenshotAs(OutputType.FILE); 
	    FileUtils.copyFile(src, new File("./test-output/ScreenCapture/"+ fileName+".png"));

	}
  }

