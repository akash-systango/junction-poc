package TestNG;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;


import static org.testng.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterTest;

public class DeleteSensor {
public static WebDriver driver;
@BeforeClass
public void initialsetup() {
	driver=new FirefoxDriver();
	driver.get("https://dashboard-staging.junction.com/#/signin?token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJha2FzaEBzeXN0YW5nby5jb20iLCJleHAiOjE0NzM0OTg5MzMsImlzcyI6Imh0dHBzOi8vY2xvdWQuanVuY3Rpb24uY29tLyIsImlhdCI6MTQ3MjIwMjkzMywidHlwIjoiSldUIiwiYXVkIjoiYUxiS0xYSFNjeW94Tmxhd1ExTDlPYlNaSXZPa29HRkUifQ.FoCeVUsfpl5272apsxfhoNHJwWFngb5b9Wk5ZAcJEhQ");
	driver.manage().window().maximize();
	driver.manage().timeouts().implicitlyWait(50,TimeUnit.SECONDS);
		driver.findElement(By.xpath("//h1[text()='Properties']"));
    List<WebElement> lst=driver.findElements(By.xpath("//div[@class='src-components-media-ImageRatio-stretch-l_6CY']"));
    lst.get(0).click();
 
    }



  @Test
  public void VerifyDeleteSensor() throws IOException {
	  List<WebElement> lst2=driver.findElements(By.xpath("//div[@class='src-components-sensors-LargeSensorCard-root-1WHCC']"));
		lst2.get(6).click();
		driver.findElement(By.xpath("//a[text()='Delete']")).click();
		driver.manage().timeouts().implicitlyWait(50,TimeUnit.SECONDS);
		driver.findElement(By.xpath("//a[text()='Yes']")).click();
		driver.manage().timeouts().implicitlyWait(50,TimeUnit.SECONDS);
		WebElement ele3= driver.findElement(By.xpath("//h2[contains(text(),'systango')]"));
		captureScreenshot("DeleteSensor");
		assertTrue(ele3.isDisplayed());
		System.out.println("External Sensor Deleted");
  }
  


  @AfterTest
  public void afterTest() {
	  driver.quit();
  }
  public static void captureScreenshot(String fileName) throws IOException {
	  	
	    TakesScreenshot ts= (TakesScreenshot)driver;
	    File src=ts.getScreenshotAs(OutputType.FILE); 
	    FileUtils.copyFile(src, new File("./test-output/ScreenCapture/"+ fileName+".png"));

	}
}
