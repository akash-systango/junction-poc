package TestNG;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterClass;


public class DragDrop {
public static WebDriver driver;
 @BeforeClass
public void initialsetup() {
	 driver=new FirefoxDriver();
		driver.get("https://dashboard-staging.junction.com/#/signin?token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJha2FzaEBzeXN0YW5nby5jb20iLCJleHAiOjE0NzM0OTg5MzMsImlzcyI6Imh0dHBzOi8vY2xvdWQuanVuY3Rpb24uY29tLyIsImlhdCI6MTQ3MjIwMjkzMywidHlwIjoiSldUIiwiYXVkIjoiYUxiS0xYSFNjeW94Tmxhd1ExTDlPYlNaSXZPa29HRkUifQ.FoCeVUsfpl5272apsxfhoNHJwWFngb5b9Wk5ZAcJEhQ");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(50,TimeUnit.SECONDS);
			driver.findElement(By.xpath("//h1[text()='Properties']"));
	    List<WebElement> lst=driver.findElements(By.xpath("//div[@class='src-components-media-ImageRatio-stretch-l_6CY']"));
	    lst.get(0).click();
		  }
	 
	 
	  
  @Test
  public void VerifyDragDrop() throws IOException {
	  driver.findElement(By.xpath("//i[@class='blue settings icon']")).click();
		driver.findElement(By.xpath("//div[text()='Property']")).click();
		driver.findElement(By.xpath("//a[text()='Users and Escalation']")).click();
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,250)", "");
		driver.findElement(By.xpath("//h3[contains(text(),'Drag & Drop User')]"));
		WebElement Ele=driver.findElement(By.xpath("//div[@id='copy_from_list']/div[@id='179']"));
		WebElement Ele2=driver.findElement(By.xpath(".//*[@id='level-1,priority-1']"));
		Actions act=new Actions(driver);
		act.clickAndHold(Ele);
		act.moveToElement(Ele2);
		act.release();
		act.build().perform();
		WebElement Ele3= driver.findElement(By.xpath(".//*[@id='outlet']/div/div[2]/div/div[2]/div"));
        captureScreenshot("DragDrop");
        Assert.assertTrue(Ele3.isDisplayed());
        System.out.println("Drag and Drop Done");
       
  }
 

  @AfterClass
  public void afterClass() {
	  driver.quit();
  }

  public static void captureScreenshot(String fileName) throws IOException {
	  	
	    TakesScreenshot ts= (TakesScreenshot)driver;
	    File src=ts.getScreenshotAs(OutputType.FILE); 
	    FileUtils.copyFile(src, new File("./test-output/ScreenCapture/"+ fileName+".png"));

	
}
}